from django import views
from django.urls import path

from contact.views import suport, contact, date_personale


app_name = "contact"

urlpatterns = [
    path('suport/', suport, name='suport'),
    path('contact/', contact, name='contact'),
    path('contact/date_caracter_personal/', date_personale, name='date'),
]
