from django.core.validators import RegexValidator
from django.db import models

# Create your models here.


class Formular(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    telefon_regex = RegexValidator(regex=r'^\+?1?\d{9,10}$', message="Numarul de telefon trebuie introdus in formatul: '+0799999999'.")
    telefon = models.CharField(validators=[telefon_regex], max_length=10, blank=True)
    email = models.EmailField()
    titlu = models.CharField(max_length=200)
