from django import forms
from django.forms import TextInput, NumberInput, CheckboxInput

from contact.models import Formular


class ContactForm(forms.ModelForm):
    comentariu = forms.CharField(widget=forms.Textarea, required=True)

    class Meta:
        model = Formular
        fields = ['first_name', 'last_name', 'telefon', 'email', 'titlu', 'comentariu']

        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Prenume', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Nume', 'class': 'form-control'}),
            'telefon': NumberInput(attrs={'placeholder': 'Telefon', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'titlu': TextInput(attrs={'placeholder': 'Titlul mesajului', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['comentariu'].widget.attrs['placeholder'] = 'Adaugati un comentariu'
        self.fields['comentariu'].widget.attrs['class'] = 'form-control'

    def clean(self):
        cleaned_data = self.cleaned_data
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        telefon = cleaned_data.get('telefon')
        email = cleaned_data.get('email')
        titlu = cleaned_data.get('titlu')
        comentariu = cleaned_data.get('comentariu')
        # telefon_exists = None
        # if telefon_exists:
        #     telefon_invalid = "Numarul de telefon trebuie introdus in formatul:'+0799999999'"
        #     self.errors['telefon'] = self.error_class([telefon_invalid])
        return cleaned_data
