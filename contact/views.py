from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from contact.forms import ContactForm
from django.views.generic import CreateView

from contact.models import Formular


def suport(request):
    return render(request, "contact/suport.html", None)


def date_personale(request):
    return render(request, "contact/date_caracter_personal.html", None)


class CreateFormular(CreateView):
    form_class = ContactForm
    model = Formular
    template_name = 'contact/contact.html'


def contact(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data.get('first_name', '')
            last_name = form.cleaned_data.get('last_name', '')
            telefon = form.cleaned_data.get('telefon', '')
            from_email = form.cleaned_data.get('email', '')
            subject = form.cleaned_data.get('titlu', '')
            message = form.cleaned_data.get('comentariu', '')
            try:
                send_mail(
                    subject,
                    message,
                    from_email,
                    # first_name,
                    # last_name,
                    # telefon,
                    # titlu,
                    ['shoesurbangear@gmail.com'],
                )
            except BadHeaderError:
                    return HttpResponse('Camp invalid')
            return render(request, "contact/succes.html", None)
    return render(request, "contact/contact.html", {'form': form})
