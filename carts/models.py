from django.contrib.auth.models import User
from django.db import models

# Create your models here.

# from URBAN_SHOES.MAIN.models import Produs
from MAIN.models import Produs
from django.db.models.signals import post_save
from django.dispatch import receiver

status_ch = (('trimisa', 'trimisa'), ('procesata', 'procesata'), ('finalizata', 'finalizata'),
             ('livrata', 'livrata'), ('anulata', 'anulata'))


class Order(models.Model):
    objects = None
    status = models.CharField(max_length=50, choices=status_ch)
    total = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    nr_comanda = models.IntegerField(default=0)
    data_finalizare_comanda = models.DateField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.status


class Carts(models.Model):
    produse = models.ForeignKey(Produs, on_delete=models.CASCADE)
    pret = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    data_creare = models.TimeField(auto_now_add=True, auto_now=False)
    update_data_creare = models.TimeField(auto_now_add=False, auto_now=True)
    activ = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    cantitate = models.IntegerField(default=1)
    objects = None

    class Meta:
        verbose_name_plural = "Carts"
