from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse
from django.views.generic import UpdateView, CreateView, ListView

from .models import Carts, Order
from MAIN.models import Produs


def view_cart(request):
    cart = Carts.objects.filter(user_id=request.user.id).filter(order_id=None)
    suma = 0
    for item in Carts.objects.filter(user_id=request.user.id, order_id=None):
        suma += float(item.pret) * float(item.cantitate)
    total_pret = float(suma)
    return render(request, "cos_produse/cos.html", {"cart": cart, "pret_total": total_pret})


@login_required
def add_to_cart(request):
    instance = Carts()
    instance.produse_id = Produs.objects.get(id=request.GET.get('id_prod')).id
    instance.user_id = request.user.id
    instance.pret = Produs.objects.get(id=request.GET.get('id_prod')).pret
    instance.cantitate = 1
    instance.save()
    if "home" in request.path:
        return redirect('Produs:home')
    elif "cautare_avansata" in request.path:
        return redirect('Produs:avansata')
    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


def orders(request):
    order = Order()
    order.status = 'trimisa'
    order.nr_comanda = Order.objects.all().values_list('nr_comanda', flat=True).last()
    order.nr_comanda += 1
    order.save()
    total = Carts.objects.filter(user_id=request.user.id, order_id=None).update(order_id=order.id)
    suma = 0
    for item in Carts.objects.filter(user_id=request.user.id, order_id=order.id):
        suma += float(item.pret) * float(item.cantitate)
    order.total = float(suma)
    order.save()
    return redirect('carts:OrderUpdate', order.id)


@login_required
def remove_from_cart(request, pk):
    try:
        remove = Carts.objects.get(id=pk).delete()
        cart = Carts.objects.filter(user_id=request.user.id, order_id=None)
    except:
        pass
    return redirect('carts:cos')


class CartUpdateView(LoginRequiredMixin, UpdateView):
    fields = "__all__"
    model = Order
    template_name = 'cos_produse/cos.html'

    def get_context_data(self, **kwargs):
        data = super(CartUpdateView, self).get_context_data(**kwargs)
        data['cart'] = Carts.objects.filter(order_id=self.kwargs['pk'])
        data['total'] = Order.objects.get(id=self.kwargs['pk']).total
        return data

    def get_queryset(self):
        return Order.objects.all()

    def get_success_url(self):
        return reverse('carts:OrderUpdate', args=[str(self.object.id)])


class ComenziList(ListView):
    model = Order
    template_name = 'cos_produse/istoric.html'
    context_object_name = 'id'

    def get_queryset(self):
        cart_user = Carts.objects.filter(user_id=self.request.user.id)
        return Order.objects.filter(id__in=cart_user.values_list('order_id', flat=True))
