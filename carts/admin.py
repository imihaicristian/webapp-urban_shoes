from django.contrib import admin

# Register your models here.

from .models import Carts, Order


class CartAdmin(admin.ModelAdmin):
    class Meta:
        model = Order


admin.site.register(Order, CartAdmin)
