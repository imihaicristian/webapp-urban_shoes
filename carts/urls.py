from django.urls import path

from . import views
from .views import view_cart, add_to_cart, orders, CartUpdateView, remove_from_cart, ComenziList

app_name = "carts"

urlpatterns = [
    path('cos/', view_cart, name='cos'),
    path('cart/', add_to_cart, name='cart'),
    path('order/', orders, name='order'),
    path('history/<int:pk>/', CartUpdateView.as_view(), name='OrderUpdate'),
    path('remove/<int:pk>/', remove_from_cart, name='remove'),
    path('istoric/', ComenziList.as_view(), name='IstoricComenzi'),
]
