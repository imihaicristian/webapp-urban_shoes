import django_filters
from django_filters.widgets import RangeWidget

from MAIN.models import Produs

# mostenire clasa RangeWidget pentru a avea placeholder pe ambele field-uri


class PretWidget(RangeWidget):
    def __init__(self, attrs=None):
        super().__init__(attrs)
        self.widgets[0].attrs.update({'placeholder': 'min'})
        self.widgets[1].attrs.update({'placeholder': 'max'})


class ProdusFilter(django_filters.FilterSet):
    pret = django_filters.RangeFilter(widget=PretWidget)
    brand = django_filters.CharFilter(lookup_expr='icontains')
    cod = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Produs
        fields = ['gen', 'brand', 'culoare', 'pret', 'marimi', 'cod']
