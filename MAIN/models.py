from django.db import models

# Create your models here.
from multiselectfield import MultiSelectField

genchoices = (('BARBATI', 'BARBATI'), ('FEMEI', 'FEMEI'), ('COPII', 'COPII'))

culoarechoices = (('alb', 'alb'), ('albastru', 'albastru'), ('argintiu', 'argintiu'), ('bej', 'bej'),
                  ('bleumarin', 'bleumarin'), ('galben', 'galben'), ('gri', 'gri'),
                  ('mov', 'mov'), ('multicolor', 'multicolor'), ('negru', 'negru'),
                  ('rosu', 'rosu'), ('roz', 'roz'), ('verde', 'verde'), ('violet', 'violet'),
                  ('visiniu', 'visiniu'))

size = (('BARBATI', (('40', '40'), ('40.5', '40.5'), ('41', '41'), ('41.5', '41.5'), ('42', '42'),
                     ('42.5', '42.5'), ('43', '43'), ('43.5', '43.5'), ('44', '44'), ('44.5', '44.5'),
                     ('45', '45'), ('45.5', '45.5'), ('46', '46'), )),
        ('FEMEI', (('36', '36'), ('36.5', '36.5'), ('37', '37'), ('37.5', '37.5'), ('38', '38'),
                   ('38.5', '38.5'), ('39', '39'), ('39.5', '39.5'), ('40', '40'), ('40.5', '40.5'),
                   ('41', '41'), )),
        ('COPII', (('20', '20'), ('21', '21'), ('22', '22'), ('23', '23'), ('24', '24'),
                   ('25', '25'), ('26', '26'), ('26.5', '26.5'), ('27', '27'), ('27.5', '27.5'),
                   ('28', '28'), ('28.5', '28.5'), ('29', '29'), ('29.5', '29.5'), ('30', '30'),
                   ('30.5', '30.5'), ('31', '31'), ('31.5', '31.5'), ('32', '32'), ('32.5', '32.5'),
                   ('33', '33'), ('33.5', '33.5'), ('34', '34'), )))


class Produs(models.Model):

    objects = None
    img = models.ImageField(upload_to='pics')
    brand = models.CharField(max_length=100)
    culoare = models.CharField(max_length=20, choices=culoarechoices)
    cod = models.CharField(max_length=100, unique=True)
    marimi = MultiSelectField(choices=size, max_length=100)
    pret = models.DecimalField(max_digits=6, decimal_places=2)
    gen = models.CharField(max_length=10, choices=genchoices)

    def __str__(self):
        return self.cod

# scrie in meniul de admin cu un singur s
    class Meta:
        verbose_name_plural = "Produs"






