# Generated by Django 3.0.6 on 2020-06-30 18:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('MAIN', '0008_auto_20200621_2324'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='produs',
            options={'verbose_name_plural': 'Produs'},
        ),
    ]
