from django.shortcuts import render
from django.views.generic import CreateView, ListView

from .models import Produs

from .filters import ProdusFilter

# Create your views here.
from carts.models import Carts


class Home(CreateView):
    model = Produs
    template_name = 'index.html'
    fields = "__all__"

    def get_context_data(self, **kwargs):
        data = super(Home, self).get_context_data(**kwargs)
        if self.request.GET.get("var"):
            data['orice'] = self.model.objects.filter(gen=self.request.GET.get("var"))
        else:
            data['orice'] = self.model.objects.filter(gen="BARBATI")
        data['gen'] = self.request.GET.get("var")
        suma = 0
        for item in Carts.objects.filter(order_id=None, user_id=self.request.user.id):
            suma += item.pret
        data['pret'] = suma
        return data


class BrandsList(ListView):
    model = Produs
    template_name = 'brands.html'

    def get_context_data(self, **kwargs):
        data = super(BrandsList, self).get_context_data(**kwargs)
        data['object_list'] = self.model.objects.filter(cod__startswith=self.request.GET.get('cod'))
        return data


def produs_list(request):
    f = ProdusFilter(request.GET, queryset=Produs.objects.all())
    return render(request, 'cautare_avansata.html', {'filter': f})
