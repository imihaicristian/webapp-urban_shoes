from django.urls import path

from . import views

app_name = "Produs"

urlpatterns = [
    path('home/', views.Home.as_view(), name='home'),
    path('brands/', views.BrandsList.as_view(), name='brands'),
    path('cautare_avansata/', views.produs_list, name='avansata'),
]
