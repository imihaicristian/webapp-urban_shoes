from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Account(User):
    check_box = models.BooleanField(default=False)
