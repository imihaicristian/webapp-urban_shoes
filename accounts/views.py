from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import CreateView, UpdateView
from .models import Account
from .forms import NewAccountForm


class CreateNewAccount(CreateView):
    form_class = NewAccountForm
    model = Account
    template_name = 'registration/registration_test.html'

    def get_form_kwargs(self):
        kwargs = super(CreateNewAccount, self).get_form_kwargs()
        kwargs.update({'pk': None, 'state': 'create'})
        return kwargs

    def get_success_url(self):
        return reverse('login')


def termene(request):
    return render(request, "registration/terms.html", None)


class ModifyUserAccount(LoginRequiredMixin, UpdateView):
    form_class = NewAccountForm
    model = User
    template_name = 'registration/modify_registration_data.html'

    def get_queryset(self):
        return User.objects.filter(id=self.request.user.id)

    def get_form_kwargs(self):
        kwargs = super(ModifyUserAccount, self).get_form_kwargs()
        kwargs.update({'pk': self.kwargs['pk'], 'state': 'update'})
        return kwargs

    def get_success_url(self):
        return reverse('Produs:home')
