from django import views
from django.urls import path
from .views import CreateNewAccount, termene, ModifyUserAccount

app_name = "accounts"

urlpatterns = [
    path('register/', CreateNewAccount.as_view(), name='new_user'),
    path('register/terms/', termene, name='terms'),
    path('register/updateProfile/<int:pk>/', ModifyUserAccount.as_view(), name='update_profile'),
]
