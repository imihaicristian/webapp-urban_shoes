from django import forms
from django.contrib.auth.models import User
from django.forms import TextInput, CheckboxInput, PasswordInput
from .models import Account


class NewAccountForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Account
        fields = ['first_name', 'last_name', 'email', 'username',
                  'password', 'confirm_password', 'check_box']

        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Prenume', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Nume', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Email', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Username', 'class': 'form-control'}),
            'password': PasswordInput(attrs={'placeholder': 'Parola', 'class': 'form-control'}),
            'check_box': CheckboxInput(attrs={'class': 'form-control', 'align': 'left'}),
        }

    def __init__(self, pk, state, *args, **kwargs):
        super(NewAccountForm, self).__init__(*args, **kwargs)
        self.pk = pk
        self.state = state
        self.fields['password'].label = 'Introduceti o parola'
        self.fields['confirm_password'].widget.attrs['placeholder'] = 'Confirmati Parola'
        self.fields['confirm_password'].widget.attrs['class'] = 'form-control'

    def clean(self):
        cleaned_data = self.cleaned_data
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        email = cleaned_data.get('email')
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        check_box = cleaned_data.get('check_box')
        user_exists = None
        email_exists = None
        if self.state == 'create':
            user_exists = User.objects.filter(username=username).exists()
            email_exists = User.objects.filter(email=email).exists()
        elif self.state == 'update':
            user_exists = User.objects.filter(username=username).exclude(id=self.pk).exists()
            email_exists = User.objects.filter(email=email).exclude(id=self.pk).exists()
        if user_exists:
            username_invalid = 'Username exista'
            self.errors['username'] = self.error_class([username_invalid])
        if email_exists:
            email_invalid = 'Adresa de email deja exista'
            self.errors['email'] = self.error_class([email_invalid])
        if confirm_password != password:
            password_invalid = 'Parola nu corespunde'
            self.errors['confirm_password'] = self.error_class([password_invalid])
        if check_box is False:
            check_box_invalid = 'Termeni neacceptati'
            self.errors['check_box'] = self.error_class([check_box_invalid])
        return cleaned_data

# salveaza hashed password
    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user
